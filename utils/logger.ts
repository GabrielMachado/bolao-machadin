interface LoggerParams {
  context?: string;
  message?: string;
  details?: any;
}

const detailsParser = (details: any) => {
  if (!details) return "";
  if (details instanceof Error) {
    return JSON.stringify({
      message: details.message,
      stack: details.stack,
      name: details.name,
      cause: details.cause,
    });
  }

  const detailsParsed =
    typeof details === "object" ? JSON.stringify(details) : details;

  return ` [${detailsParsed}]`;
};

const parseMessage = (
  params: LoggerParams,
  type: "INFO" | "ERROR" | "WARNING"
) => {
  const { context, details, message } = params;
  const contextParsed = !!context ? `${context?.toUpperCase()}` : `NO_CONTEXT`;
  const messageParsed = `[${type}] [${contextParsed}] [${message}]:${detailsParser(
    details
  )}`;

  return messageParsed;
};

export const logger = {
  info: (params: LoggerParams) => {
    console.info(parseMessage(params, "INFO"));
  },
  warn: (params: LoggerParams) => {
    console.warn(parseMessage(params, "WARNING"));
  },
  error: (params: LoggerParams) => {
    console.error(parseMessage(params, "ERROR"));
  },
};
