import { withIronSessionSsr } from "iron-session/next";
import { GetServerSidePropsContext } from "next";
import PageWrapper from "../../view/wrappers/PageWrapper";
import SignInContainer from "../../view/containers/auth/SignInContainer";
import { IUser } from "../../domain";
import { sessionOptions } from "../../http/controllers/ironSession";
interface SignInPageProps {
  user?: IUser;
  [key: string]: any;
}

export const getServerSideProps = withIronSessionSsr<SignInPageProps>(
  async (context: GetServerSidePropsContext) => {
    const user = context.req.session.user;

    if (!!user?.id)
      return {
        props: {
          user,
        },
        redirect: {
          destination: "/competition/list",
          permanent: true,
        },
      };

    return {
      props: {},
    };
  },
  sessionOptions
);

const SignInPage = (props: SignInPageProps) => {
  return (
    <PageWrapper>
      <SignInContainer />
    </PageWrapper>
  );
};

export default SignInPage;
