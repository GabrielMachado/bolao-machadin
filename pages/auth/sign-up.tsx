import { withIronSessionSsr } from "iron-session/next";
import { GetServerSidePropsContext, NextPage } from "next";
import { IUser } from "../../domain";
import { sessionOptions } from "../../http/controllers/ironSession";
import SignUpContainer from "../../view/containers/auth/SignUpContainer";
import PageWrapper from "../../view/wrappers/PageWrapper";

interface SignUpPageProps {
  user?: IUser;
  [key: string]: any;
}

export const getServerSideProps = withIronSessionSsr<SignUpPageProps>(
  async (context: GetServerSidePropsContext) => {
    const user = context.req.session.user;

    if (!!user?.id)
      return {
        props: {
          user,
        },
        redirect: {
          destination: "/competition/list",
          permanent: true,
        },
      };

    return {
      props: {},
    };
  },
  sessionOptions
);

const SignUpPage: NextPage<SignUpPageProps> = (props) => {
  return (
    <PageWrapper user={props.user}>
      <SignUpContainer />
    </PageWrapper>
  );
};

export default SignUpPage;
