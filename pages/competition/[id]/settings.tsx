import { withIronSessionSsr } from "iron-session/next";
import { GetServerSidePropsContext, NextPage } from "next";
import { IUser, IUserCompetition, UserClaim } from "../../../domain";
import { sessionOptions } from "../../../http/controllers/ironSession";
import SettingsUserCompetitionContainer from "../../../view/containers/competition/SettingCompetitionContainer";
import PageWrapper from "../../../view/wrappers/PageWrapper";

interface UserCompetitionPageProps {
  user?: IUser;
  competitionId: IUserCompetition["id"];
  [key: string]: any;
}

export const getServerSideProps = withIronSessionSsr<UserCompetitionPageProps>(
  async (context: GetServerSidePropsContext) => {
    const user = context.req.session.user;
    const competitionId = Number(context.query.id as string);

    if (!user)
      return {
        props: {},
        redirect: { destination: "/auth/sign-in", permanent: true },
      };

    if (user.claim !== UserClaim.ADMIN)
      return {
        props: {},
        redirect: { destination: `/competition/list`, permanent: true },
      };

    return {
      props: {
        user,
        competitionId,
      },
    };
  },
  sessionOptions
);

const UserCompetitionPage: NextPage<UserCompetitionPageProps> = (props) => {
  return (
    <PageWrapper user={props.user}>
      <SettingsUserCompetitionContainer competitionId={props.competitionId} />
    </PageWrapper>
  );
};

export default UserCompetitionPage;
