import { withIronSessionSsr } from "iron-session/next";
import { GetServerSidePropsContext, NextPage } from "next";
import { IUser } from "../../domain";
import { sessionOptions } from "../../http/controllers/ironSession";
import ListCompetitionContainer from "../../view/containers/competition/ListCompetitionContainer";
import PageWrapper from "../../view/wrappers/PageWrapper";

interface ListCompetitionPageProps {
  user?: IUser;
  [key: string]: any;
}

export const getServerSideProps = withIronSessionSsr<ListCompetitionPageProps>(
  async (context: GetServerSidePropsContext) => {
    const user = context.req.session.user;
    if (!user)
      return {
        props: {},
        redirect: { destination: "/auth/sign-in", permanent: true },
      };

    return {
      props: {
        user,
      },
    };
  },
  sessionOptions
);

const ListCompetitionPage: NextPage<ListCompetitionPageProps> = (props) => {
  return (
    <PageWrapper user={props.user}>
      <ListCompetitionContainer user={props.user!} />
    </PageWrapper>
  );
};

export default ListCompetitionPage;
