import type { NextPage } from "next";
import HomeContainer from "../view/containers/home";
import PageWrapper from "../view/wrappers/PageWrapper";

const Home: NextPage = () => {
  return (
    <PageWrapper>
      <HomeContainer />
    </PageWrapper>
  );
};

export default Home;
