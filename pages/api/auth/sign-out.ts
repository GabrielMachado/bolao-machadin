import { sessionOptions } from "../../../http/controllers/ironSession";
import { withIronSessionApiRoute } from "iron-session/next";
import controllerHandler from "../../../http/middlewares/controllerHandler";
import signOutController from "../../../http/controllers/auth/signOutController";

export default withIronSessionApiRoute(
  (req, res) => controllerHandler(req, res, signOutController),
  sessionOptions
);
