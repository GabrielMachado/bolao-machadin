import { sessionOptions } from "../../../http/controllers/ironSession";
import { withIronSessionApiRoute } from "iron-session/next";
import controllerHandler from "../../../http/middlewares/controllerHandler";
import signInController from "../../../http/controllers/auth/signInController";

export default withIronSessionApiRoute(
  (req, res) => controllerHandler(req, res, signInController),
  sessionOptions
);
