import { sessionOptions } from "../../../../http/controllers/ironSession";
import { withIronSessionApiRoute } from "iron-session/next";
import controllerHandler from "../../../../http/middlewares/controllerHandler";
import updateManyUserCompetitionBetController from "../../../../http/controllers/bet/updateManyUserCompetitionBetController";
import { UserClaim } from "../../../../domain";

export default withIronSessionApiRoute(
  (req, res) =>
    controllerHandler(req, res, updateManyUserCompetitionBetController, [
      UserClaim.USER,
      UserClaim.ADMIN,
    ]),
  sessionOptions
);
