import { sessionOptions } from "../../../../http/controllers/ironSession";
import { withIronSessionApiRoute } from "iron-session/next";
import controllerHandler from "../../../../http/middlewares/controllerHandler";
import listUserCompetitionBetByUserCompetitionIdController from "../../../../http/controllers/bet/listUserCompetitionBetByUserCompetitionIdController";
import { UserClaim } from "../../../../domain";

export default withIronSessionApiRoute(
  (req, res) =>
    controllerHandler(
      req,
      res,
      listUserCompetitionBetByUserCompetitionIdController,
      [UserClaim.USER, UserClaim.ADMIN]
    ),
  sessionOptions
);
