import { sessionOptions } from "../../../http/controllers/ironSession";
import { withIronSessionApiRoute } from "iron-session/next";
import controllerHandler from "../../../http/middlewares/controllerHandler";
import listCompetitionController from "../../../http/controllers/competition/listCompetitionController";
import { UserClaim } from "../../../domain";

export default withIronSessionApiRoute(
  (req, res) =>
    controllerHandler(req, res, listCompetitionController, [
      UserClaim.USER,
      UserClaim.ADMIN,
    ]),
  sessionOptions
);
