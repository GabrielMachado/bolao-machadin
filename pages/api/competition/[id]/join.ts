import { sessionOptions } from "../../../../http/controllers/ironSession";
import joinUserCompetitionController from "../../../../http/controllers/competition/joinUserCompetitionController";
import { withIronSessionApiRoute } from "iron-session/next";
import controllerHandler from "../../../../http/middlewares/controllerHandler";
import { UserClaim } from "../../../../domain";

export default withIronSessionApiRoute(
  (req, res) =>
    controllerHandler(req, res, joinUserCompetitionController, [
      UserClaim.USER,
      UserClaim.ADMIN,
    ]),
  sessionOptions
);
