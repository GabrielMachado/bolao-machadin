import { sessionOptions } from "../../../http/controllers/ironSession";
import { withIronSessionApiRoute } from "iron-session/next";
import controllerHandler from "../../../http/middlewares/controllerHandler";
import { UserClaim } from "../../../domain";
import listUserCompetitionController from "../../../http/controllers/userCompetition/listUserCompetitionController";

export default withIronSessionApiRoute(
  (req, res) =>
    controllerHandler(req, res, listUserCompetitionController, [
      UserClaim.USER,
      UserClaim.ADMIN,
    ]),
  sessionOptions
);
