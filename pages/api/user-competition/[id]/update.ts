import { sessionOptions } from "../../../../http/controllers/ironSession";
import { withIronSessionApiRoute } from "iron-session/next";
import controllerHandler from "../../../../http/middlewares/controllerHandler";
import { UserClaim } from "../../../../domain";
import updateUserCompetitionStatusController from "../../../../http/controllers/userCompetition/updateUserCompetitionStatusController";

export default withIronSessionApiRoute(
  (req, res) =>
    controllerHandler(req, res, updateUserCompetitionStatusController, [
      UserClaim.ADMIN,
    ]),
  sessionOptions
);
