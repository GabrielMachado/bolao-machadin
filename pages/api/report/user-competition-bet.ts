import { sessionOptions } from "../../../http/controllers/ironSession";
import { withIronSessionApiRoute } from "iron-session/next";
import controllerHandler from "../../../http/middlewares/controllerHandler";
import { UserClaim } from "../../../domain";
import generateUserCompetitionBetReportByCompetitionIdController from "../../../http/controllers/report/generateUserCompetitionBetReportByCompetitionIdController";

export default withIronSessionApiRoute(
  (req, res) =>
    controllerHandler(
      req,
      res,
      generateUserCompetitionBetReportByCompetitionIdController,
      [UserClaim.ADMIN]
    ),
  sessionOptions
);
