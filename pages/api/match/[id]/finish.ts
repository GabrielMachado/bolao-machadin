import { sessionOptions } from "../../../../http/controllers/ironSession";
import { withIronSessionApiRoute } from "iron-session/next";
import controllerHandler from "../../../../http/middlewares/controllerHandler";
import finishedMatchByIdController from "../../../../http/controllers/match/finishedMatchByIdController";
import { UserClaim } from "../../../../domain";

export default withIronSessionApiRoute(
  (req, res) =>
    controllerHandler(req, res, finishedMatchByIdController, [UserClaim.ADMIN]),
  sessionOptions
);
