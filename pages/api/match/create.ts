import { sessionOptions } from "../../../http/controllers/ironSession";
import { withIronSessionApiRoute } from "iron-session/next";
import controllerHandler from "../../../http/middlewares/controllerHandler";
import createMatchController from "../../../http/controllers/match/createMatchController";
import { UserClaim } from "../../../domain";

export default withIronSessionApiRoute(
  (req, res) =>
    controllerHandler(req, res, createMatchController, [UserClaim.ADMIN]),
  sessionOptions
);
