import { sessionOptions } from "../../../http/controllers/ironSession";
import createUserController from "../../../http/controllers/user/createuserController";
import { withIronSessionApiRoute } from "iron-session/next";
import controllerHandler from "../../../http/middlewares/controllerHandler";

export default withIronSessionApiRoute(
  (req, res) => controllerHandler(req, res, createUserController),
  sessionOptions
);
