import { withIronSessionSsr } from "iron-session/next";
import { GetServerSidePropsContext, NextPage } from "next";
import { IUser, IUserCompetition } from "../../../domain";
import { sessionOptions } from "../../../http/controllers/ironSession";
import ShowUserCompetitionContainer from "../../../view/containers/userCompetition/ShowUserCompetitionContainer";
import PageWrapper from "../../../view/wrappers/PageWrapper";

interface UserCompetitionPageProps {
  user?: IUser;
  userCompetitionId: IUserCompetition["id"];
  [key: string]: any;
}

export const getServerSideProps = withIronSessionSsr<UserCompetitionPageProps>(
  async (context: GetServerSidePropsContext) => {
    const user = context.req.session.user;
    const userCompetitionId = Number(context.query.id as string);

    if (!user)
      return {
        props: {},
        redirect: { destination: "/auth/sign-in", permanent: true },
      };

    return {
      props: {
        user,
        userCompetitionId,
      },
    };
  },
  sessionOptions
);

const UserCompetitionPage: NextPage<UserCompetitionPageProps> = (props) => {
  return (
    <PageWrapper user={props.user}>
      <ShowUserCompetitionContainer
        userCompetitionId={props.userCompetitionId}
      />
    </PageWrapper>
  );
};

export default UserCompetitionPage;
