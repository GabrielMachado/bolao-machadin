import { NextApiRequest, NextApiResponse } from "next";
import createMatchUsecase from "../../../usecases/match/createMatchUsecase";
import parseBody from "../parseBody";

const createMatchController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const body = parseBody(req.body);

  const result = await createMatchUsecase(body);

  return result;
};

export default createMatchController;
