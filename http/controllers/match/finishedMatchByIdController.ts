import { NextApiRequest, NextApiResponse } from "next";
import finishedMatchByIdUsecase from "../../../usecases/match/finishedMatchByIdUsecase";
import parseBody from "../parseBody";

const finishedMatchByIdController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const id = req.query.id as string;
  const body = parseBody(req.body);

  const result = await finishedMatchByIdUsecase(Number(id), body);

  return result;
};

export default finishedMatchByIdController;
