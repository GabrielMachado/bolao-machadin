import { NextApiRequest, NextApiResponse } from "next";
import { IUser } from "../../../domain";
import parseBody from "../parseBody";
import createUserUsecase from "../../../usecases/user/createUserUsecase";

const createUserController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const body: Pick<IUser, "name" | "email" | "password"> = parseBody(req.body);

  const userCreated = await createUserUsecase(body);

  return userCreated;
};

export default createUserController;
