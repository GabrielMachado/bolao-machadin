import { IronSessionOptions } from "iron-session";
import { IUser } from "../../domain";

export const sessionOptions: IronSessionOptions = {
  cookieName: "api-auth",
  password: process.env.IRON_SESSION_SALT_KEY || "",
};

declare module "iron-session" {
  interface IronSessionData {
    user?: IUser;
  }
}
