import { NextApiRequest, NextApiResponse } from "next";
import generateUserCompetitionBetReportByCompetitionIdUsecase from "../../../usecases/report/generateUserCompetitionBetReportByCompetitionIdUsecase";

const generateUserCompetitionBetReportByCompetitionIdController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const query = req.query;
  const competitionId = Number.parseInt(query.competitionId as string);
  return generateUserCompetitionBetReportByCompetitionIdUsecase(competitionId);
};

export default generateUserCompetitionBetReportByCompetitionIdController;
