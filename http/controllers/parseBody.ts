const parseBody = (body: Object | string) => {
  if (typeof body === "object") return body;
  return JSON.parse(body);
};

export default parseBody;
