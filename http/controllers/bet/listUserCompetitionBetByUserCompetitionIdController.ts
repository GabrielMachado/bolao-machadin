import { NextApiRequest, NextApiResponse } from "next";
import listUserCompetitionBetByUserCompetitionIdUsecase from "../../../usecases/bet/listUserCompetitionBetByUserCompetitionIdUsecase";
import { parsePagination } from "../utils";

const listUserCompetitionBetByUserCompetitionIdController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const query = req.query;
  const pagination = parsePagination(req);
  const userCompetitionId = query.userCompetitionId as string;
  const user = req.session.user!;

  const result = await listUserCompetitionBetByUserCompetitionIdUsecase(
    Number.parseInt(userCompetitionId),
    {
      ...query,
      ...pagination,
    },
    user
  );
  return result;
};

export default listUserCompetitionBetByUserCompetitionIdController;
