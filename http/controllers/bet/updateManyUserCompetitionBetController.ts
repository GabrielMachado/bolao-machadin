import { NextApiRequest, NextApiResponse } from "next";
import { updateManyUserCompetitionBetUsecase } from "../../../usecases/bet/updateManyUserCompetitionBetUsecase";
import parseBody from "../parseBody";

const updateManyUserCompetitionBetController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const query = req.query;
  const body = parseBody(req.body);
  const userCompetitionId = query.userCompetitionId as string;
  const user = req.session.user!;

  return updateManyUserCompetitionBetUsecase(
    Number.parseInt(userCompetitionId),
    body,
    user
  );
};

export default updateManyUserCompetitionBetController;
