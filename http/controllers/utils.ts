import { NextApiRequest } from "next";

export const parsePagination = (req: NextApiRequest) => {
  const page = !!req.query.page
    ? Number.parseInt(req.query.page as string)
    : undefined;
  const limit = !!req.query.limit
    ? Number.parseInt(req.query.limit as string)
    : undefined;

  return { page, limit };
};
