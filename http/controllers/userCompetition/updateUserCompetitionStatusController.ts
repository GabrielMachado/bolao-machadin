import { NextApiRequest, NextApiResponse } from "next";
import updateUserCompetitionStatusUsecase from "../../../usecases/userCompetition/updateUserStatusUsecase";
import parseBody from "../parseBody";

const updateUserCompetitionStatusController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const userCompetitionId = req.query.id;
  const body = parseBody(req.body);

  const result = await updateUserCompetitionStatusUsecase(
    Number(userCompetitionId),
    body.status
  );
  return result;
};

export default updateUserCompetitionStatusController;
