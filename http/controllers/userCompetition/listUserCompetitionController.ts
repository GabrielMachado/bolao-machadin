import { NextApiRequest, NextApiResponse } from "next";
import listUserCompetitionUsecase from "../../../usecases/userCompetition/listUserCompetitionUsecase";
import { parsePagination } from "../utils";

const listUserCompetitionController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const query = req.query;
  const pagination = parsePagination(req);
  const user = req.session.user!;

  const result = await listUserCompetitionUsecase(
    {
      ...query,
      ...pagination,
    },
    user
  );
  return result;
};

export default listUserCompetitionController;
