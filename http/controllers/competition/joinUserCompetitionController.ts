import { NextApiRequest, NextApiResponse } from "next";
import { joinCompetitionUsecase } from "../../../usecases/competition/joinUserCompetitionUsecase";

const joinUserCompetitionController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const competitionId = req.query.id;
  const user = req.session.user!;

  const userCompetition = await joinCompetitionUsecase(
    user.id,
    Number(competitionId)
  );

  return userCompetition;
};

export default joinUserCompetitionController;
