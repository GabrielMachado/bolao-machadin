import { NextApiRequest, NextApiResponse } from "next";
import listCompetitionUsecase from "../../../usecases/competition/listCompetitionUsecase";
import { parsePagination } from "../utils";

const listCompetitionController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const user = req.session.user!;
  const query = req.query;
  const pagination = parsePagination(req);

  const result = await listCompetitionUsecase(
    {
      ...query,
      ...pagination,
    },
    user
  );
  return result;
};

export default listCompetitionController;
