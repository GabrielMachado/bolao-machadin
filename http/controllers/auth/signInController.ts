import { NextApiRequest, NextApiResponse } from "next";
import { ForbiddenError, IUser } from "../../../domain";
import getOneUserByEmailUsecase from "../../../usecases/user/getOneUserByEmailUsecase";
import parseBody from "../parseBody";
import bcrypt from "bcrypt";

async function signInController(req: NextApiRequest, res: NextApiResponse) {
  const body: Pick<IUser, "email" | "password"> = parseBody(req.body);

  const user = await getOneUserByEmailUsecase(body.email);

  const result = await bcrypt.compare(body.password, user.password);
  if (!result) throw new ForbiddenError("wrong password");

  req.session.user = user;
  await req.session.save();

  return user;
}

export default signInController;
