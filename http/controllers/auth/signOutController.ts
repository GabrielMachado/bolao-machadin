import { NextApiRequest, NextApiResponse } from "next";

const signOutController = async (req: NextApiRequest, res: NextApiResponse) => {
  req.session.destroy();
  res.json({ ok: true });
};

export default signOutController;
