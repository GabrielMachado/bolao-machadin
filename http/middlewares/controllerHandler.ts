import { NextApiRequest, NextApiResponse } from "next";
import initDB from "../../database/initDB";
import { UserClaim, ForbiddenError } from "../../domain";
import { BaseError } from "../../domain/errors/BaseError";
import { logger } from "../../utils/logger";

const controllerHandler = async (
  req: NextApiRequest,
  res: NextApiResponse,
  next: (req: NextApiRequest, res: NextApiResponse) => Promise<any>,
  allowedClaims?: UserClaim[]
) => {
  try {
    initDB();
    const user = req.session.user;
    if (
      !!allowedClaims &&
      (!user?.claim || !allowedClaims.includes(user.claim))
    )
      throw new ForbiddenError("Access denied");

    const result = await next(req, res);
    return res.status(200).send(result);
  } catch (error) {
    logger.error({
      context: "controller handler",
      details: error,
      message: "could not execute request properly",
    });
    if (error instanceof BaseError)
      return res.status(error.getStatus()).send({
        message: error.message,
      });

    if (error instanceof Error)
      return res.status(500).send({
        message: error.message,
      });

    return res.status(500).send({ message: "Internal Server Error" });
  }
};

export default controllerHandler;
