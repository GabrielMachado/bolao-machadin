import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import AccountCircle from "@mui/icons-material/AccountCircle";
import { IUser } from "../../../domain";
import { useState } from "react";
import { useRouter } from "next/router";
import style from "./Header.module.css";
import Link from "next/link";

export interface HeaderProps {
  user?: IUser;
}

export const Header = (props: HeaderProps) => {
  const isLogged = !!props.user?.id;
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const router = useRouter();

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const signOut = async () => {
    handleClose();
    const result = await fetch("/api/auth/sign-out", { method: "POST" });
    if (!result.ok) return;

    router.push("/auth/sign-in");
  };

  const signIn = () => {
    handleClose();
    router.push("/auth/sign-in");
  };

  const signUp = () => {
    handleClose();
    router.push("/auth/sign-up");
  };

  const listCompetitions = () => {
    handleClose();
    router.push("/competition/list");
  };

  const goToHome = () => {
    handleClose();
    router.push("/");
  };

  return (
    <nav className={style["header-container"]}>
      <div className={style["header-viewport"]}>
        <div className={style["header-column"]} />
        <div className={style["header-column"]}>
          <Typography
            onClick={() => goToHome()}
            className={style["header-title"]}
          >
            BOLAO MACHADIN
          </Typography>
        </div>
        <div className={style["header-menu-container"]}>
          <IconButton
            size="large"
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleMenu}
            color="inherit"
          >
            <AccountCircle />
          </IconButton>

          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            keepMounted
            transformOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            {!isLogged && <MenuItem onClick={() => signIn()}>Entrar</MenuItem>}
            {!isLogged && (
              <MenuItem onClick={() => signUp()}>Cadastrar</MenuItem>
            )}
            {isLogged && (
              <MenuItem onClick={() => listCompetitions()}>Bolões</MenuItem>
            )}
            {isLogged && <MenuItem onClick={() => signOut()}>Sair</MenuItem>}
          </Menu>
        </div>
      </div>
    </nav>
  );
};

export default Header;
