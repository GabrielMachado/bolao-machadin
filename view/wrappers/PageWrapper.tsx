import { FetchProvider } from "../hooks/useFetch";
import { LoadingProvider } from "../hooks/useLoading";
import { NotifyProvider } from "../hooks/useNotify";
import styles from "./PageWrapper.module.css";
import { IUser } from "../../domain";
import Header from "../components/layout/Header";

interface PageWrapperProps {
  children: JSX.Element;
  user?: IUser;
}

const PageWrapper = (props: PageWrapperProps) => {
  return (
    <LoadingProvider>
      <NotifyProvider>
        <FetchProvider>
          <div className={styles["wrapper"]}>
            <Header user={props.user} />

            <div className={styles["body-content"]}>
              <div className={styles["viewport"]}>{props.children}</div>
            </div>
          </div>
        </FetchProvider>
      </NotifyProvider>
    </LoadingProvider>
  );
};

export default PageWrapper;
