import { Card, CardContent, Typography } from "@mui/material";
import Link from "next/link";
import styles from "./Home.module.css";

const HomeContainer = () => {
  return (
    <Card className={styles["home-container"]}>
      <CardContent className={styles["card-content"]}>
        <section className={styles["section"]}>
          <Typography variant="h6">Sobre o bolão</Typography>

          <Typography variant="body1">
            O bolão do hexa irá contar com 23 jogos selecionados entre os
            melhores e/ou mais importantes da fase de grupos, mais todos os
            outros 15 da fase mata a mata.
          </Typography>

          <Typography variant="body1">
            Após o fim da primeira fase, já no dia 02/12/2022, será
            disponibilizada o restante dos jogos com as oitavas de final, na
            qual cada participante terá um dia para preencher e enviar, antes do
            primeiro jogo das oitavas começar, e assim será feito para as fases
            de quartas, semi e final.
          </Typography>

          <Typography variant="body1">
            Todos são bem-vindos, afinal ninguém é especialista, estamos aqui
            pra se divertir, que ganhe o mais Sortudo!!!
          </Typography>
        </section>

        <section className={styles["section"]}>
          <Typography variant="h6">Inscrição</Typography>
          <Typography variant="body1">
            O valor da inscrição é de R$ 20,00 por pessoa
          </Typography>
          <Typography variant="body1">
            Após realizar seu cadastro faça um pix para a chave abaixo e
            encaminhe o comprovante da transferência para algum dos
            organizadores, informe também o e-mail cadastrado.
          </Typography>

          <ul>
            <li>
              <Typography variant="body1">
                <b>Chave Pix:</b> 11951580323{" "}
              </Typography>
            </li>
            <li>
              <Typography variant="body1">
                <b>Nome: </b>Pedro Henrique Amaral Bidin Vargas{" "}
              </Typography>
            </li>
            <li>
              <Typography variant="body1">
                <b>Banco:</b> Pic Pay{" "}
              </Typography>
            </li>
          </ul>
        </section>

        <section className={styles["section"]}>
          <Typography variant="h6">Premiação</Typography>
          <ul>
            <li>
              <Typography variant="body1">
                <b>1º lugar:</b> 50% do Prêmio
              </Typography>
            </li>
            <li>
              <Typography variant="body1">
                <b>2º lugar:</b> 30% do Prêmio
              </Typography>
            </li>
            <li>
              <Typography variant="body1">
                <b>3º lugar:</b> 20% do Prêmio
              </Typography>
            </li>
          </ul>

          <Typography variant="body1">
            O valor do prêmio total será de acordo com quantos participantes
            houver e será anunciado após o último dia de envio das tabelas;
          </Typography>
        </section>

        <section className={styles["section"]}>
          <Typography variant="h6">Pontuação:</Typography>

          <ul>
            <li>
              <Typography variant="body1">
                <b>Acertou a vitória/empate/derrota:</b> 1 ponto
              </Typography>
            </li>
            <li>
              <Typography variant="body1">
                <b>Acertou o placar exato:</b> 3 pontos
              </Typography>
            </li>
            <li>
              <Typography variant="body1">
                <b>Não acertou nada:</b> 0 pontos
              </Typography>
            </li>
          </ul>

          <Typography variant="body1">
            A partir das oitavas a pontuação irá aumentar 1 ponto em cada
            quesito, com resultado valendo 2 e o placar exato valendo 4, a fim
            de dar chance a quem não foi bem na primeira fase.
          </Typography>
        </section>

        <section className={styles["section"]}>
          <Typography variant="h6">Sobre a plataforma:</Typography>
          <Typography variant="body1">
            Esse site foi desenvolvido a fim de automatizar e facilitar o
            tratamento de dados.
          </Typography>
          <Typography>
            O código é aberto e pode ser visto
            <Link href="https://gitlab.com/GabrielMachado/bolao-machadin">
              {" aqui"}
            </Link>
            . Todos são bem-vindos para auditorar ou contribuir com o projeto.
          </Typography>
        </section>

        <section className={styles["section"]}>
          <Typography variant="h6">Como usar:</Typography>
          <Typography variant="body1">
            Para acessar os recursos será necessário realizar um
            <Link href="/auth/sign-up">{" cadastro"}</Link>.
          </Typography>
          <Typography variant="body1">
            Após efetuar o cadastro o usuário será redirecionado para a página
            de <Link href="/competition/list">bolões</Link>.{"\n"}
            <b>É NECESSÁRIO CLICAR NO BOTÃO ENTRAR PARA REALIZAR A INSCRIÇÃO</b>
          </Typography>
          <Typography variant="body1">
            Uma vez realizada a inscrição você já poderá começar registrar seus
            palpites!
          </Typography>
          <Typography variant="body1">
            Não pastele e fique atento às datas.
          </Typography>

          <Typography variant="body1">
            Qualquer dúvida ou problema, entrar em contato com o Bidin ou com o
            Machado.
          </Typography>
        </section>
      </CardContent>
    </Card>
  );
};

export default HomeContainer;
