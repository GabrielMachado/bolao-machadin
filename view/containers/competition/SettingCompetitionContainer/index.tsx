import { IUserCompetition } from "../../../../domain";
import { ListResponseDTO } from "../../../../domain/dtos/ListDTO";
import { useFetch } from "../../../hooks/useFetch";
import qs from "query-string";
import { useCallback, useEffect, useState } from "react";
import IconButton from "@mui/material/IconButton";
import TextField from "@mui/material/TextField";
import SearchIcon from "@mui/icons-material/Search";
import UserCompetitionRow from "./components/UserCompetitionRow";
import style from "./SettingCompetitionContainer.module.css";
import { Button } from "@mui/material";
import { useLoading } from "../../../hooks/useLoading";
import fileDownload from "js-file-download";

interface SettingsUserCompetitionContainerProps {
  competitionId: IUserCompetition["id"];
}

const SettingsUserCompetitionContainer = (
  props: SettingsUserCompetitionContainerProps
) => {
  const { fetch } = useFetch();
  const [userCompetitionList, setUserCompetitionList] = useState<
    IUserCompetition[]
  >([]);
  const [email, setEmail] = useState<string>("");
  const { loading } = useLoading();

  useEffect(() => {
    fetchUserCompetition();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchUserCompetition = useCallback(async () => {
    const query = qs.stringify({
      sortBy: "user.name",
      sortDir: "asc",
      "competition.id": props.competitionId,
      ...(!!email && { "user.email_includes": email }),
    });

    const response: ListResponseDTO<IUserCompetition> = await fetch({
      method: "GET",
      url: `/api/user-competition/list?${query}`,
    });

    if (!!response?.results) setUserCompetitionList(response.results);
  }, [props.competitionId, fetch, email]);

  const generateReport = async (fileName: string, path: string) => {
    const response = await fetch({
      method: "GET",
      url: path,
    });

    if (!(response && response.report)) return;

    const today = new Date().toISOString();

    fileDownload(response.report, `${fileName}_${today}.csv`);
  };

  return (
    <div className={style["user-competition-list"]}>
      <div className={style["reports-container"]}>
        <Button
          className={style["report-button"]}
          disabled={loading}
          size="small"
          variant="contained"
          onClick={() => {
            const query = qs.stringify({
              competitionId: props.competitionId,
            });
            generateReport(
              `palpites`,
              `/api/report/user-competition-bet?${query}`
            );
          }}
        >
          Relatório palpites
        </Button>
      </div>
      <div className={style["search-user-container"]}>
        <TextField
          value={email}
          label={"Pesquise pelo email"}
          onChange={(e) => setEmail(e.target.value)}
          size={"small"}
          style={{ flex: 1 }}
        />

        <IconButton
          size="medium"
          onClick={() => fetchUserCompetition()}
          color="inherit"
        >
          <SearchIcon></SearchIcon>
        </IconButton>
      </div>

      {userCompetitionList.map((userCompetition) => {
        return (
          <UserCompetitionRow
            key={userCompetition.id}
            userCompetition={userCompetition}
            onUpdateUserCompegionStatus={() => fetchUserCompetition()}
          />
        );
      })}
    </div>
  );
};

export default SettingsUserCompetitionContainer;
