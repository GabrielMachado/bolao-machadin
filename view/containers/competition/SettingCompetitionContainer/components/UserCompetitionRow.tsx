import { Button, Typography } from "@mui/material";
import { useCallback } from "react";
import { IUserCompetition } from "../../../../../domain";
import { useFetch } from "../../../../hooks/useFetch";
import { useLoading } from "../../../../hooks/useLoading";
import styles from "./UserCompetitionRow.module.css";

interface UserCompetitionRowProps {
  userCompetition: IUserCompetition;
  onUpdateUserCompegionStatus: () => Promise<void>;
}

const UserCompetitionRow = (props: UserCompetitionRowProps) => {
  const userCompetition = props.userCompetition;
  const statusLabel = userCompetition.status ? "Pago" : "Não Pago";
  const { loading } = useLoading();
  const { fetch } = useFetch();

  const updateUserCompetionStatus = useCallback(async () => {
    await fetch({
      method: "PUT",
      url: `/api/user-competition/${props.userCompetition.id}/update`,
      body: {
        status: !props.userCompetition.status,
      },
      successMessage: "Usuário atualizado",
    });
    await props.onUpdateUserCompegionStatus();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fetch, props.userCompetition, props.onUpdateUserCompegionStatus]);

  return (
    <div className={styles["user-competition-row-container"]}>
      <div>
        <Typography variant={"body1"}>{userCompetition.user.name}</Typography>
        <Typography variant={"caption"}>
          {userCompetition.user.email}
        </Typography>
      </div>

      <Button
        size={"small"}
        variant={"contained"}
        color={userCompetition.status ? "success" : "error"}
        disabled={loading}
        onClick={() => updateUserCompetionStatus()}
      >
        {statusLabel}
      </Button>
    </div>
  );
};

export default UserCompetitionRow;
