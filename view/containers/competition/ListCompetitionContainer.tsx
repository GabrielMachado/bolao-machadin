import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import SettingsIcon from "@mui/icons-material/Settings";
import {
  ICompetition,
  IUser,
  IUserCompetition,
  UserClaim,
} from "../../../domain";
import { ListResponseDTO } from "../../../domain/dtos/ListDTO";
import { useCallback, useEffect, useState } from "react";
import { useFetch } from "../../hooks/useFetch";
import style from "./ListCompetitionContainer.module.css";
import { parseDate } from "../../utils/date";
import { useRouter } from "next/router";

interface ListCompetitionRowProps {
  competition: ICompetition;
  user: IUser;
}

const ListCompetitionRow = (props: ListCompetitionRowProps) => {
  const { fetch } = useFetch();
  const router = useRouter();

  const joinCompetition = useCallback(async () => {
    const competition = props.competition;
    const alreadySubscribed = !!competition.userCompetitionList?.length;

    if (alreadySubscribed)
      return router.push(
        `/user-competition/${competition.userCompetitionList![0].id!}`
      );

    const result: IUserCompetition = await fetch({
      method: "POST",
      url: `/api/competition/${competition.id}/join`,
      successMessage: "Inscrição feita",
    });

    router.push(`/user-competition/${result.id}`);
  }, [fetch, props.competition, router]);

  const goToSettings = () => {
    router.push(`/competition/${props.competition.id}/settings`);
  };

  return (
    <div className={style["competition-row-container"]}>
      <div className={style["competition-row-container-info"]}>
        <div>
          <Typography variant="body1">{props.competition.name}</Typography>
        </div>
        <div>
          <Typography variant="caption">
            ({parseDate(props.competition.startDate)} -
            {parseDate(props.competition.endDate)})
          </Typography>
        </div>
      </div>

      {props.user.claim === UserClaim.ADMIN && (
        <IconButton
          onClick={() => goToSettings()}
          size={"small"}
          color={"info"}
        >
          <SettingsIcon />
        </IconButton>
      )}

      <Button
        color="success"
        size="small"
        variant="contained"
        onClick={() => joinCompetition()}
      >
        entrar
      </Button>
    </div>
  );
};

interface ListCompetitionContainerProps {
  user: IUser;
}

const ListCompetitionContainer = (props: ListCompetitionContainerProps) => {
  const [competitionList, setCompetitionList] = useState<ICompetition[]>([]);
  const { fetch } = useFetch();

  const fetchCompetitionList = useCallback(async () => {
    const response: ListResponseDTO<ICompetition> = await fetch({
      method: "GET",
      url: "/api/competition/list",
    });

    if (!!response?.results) setCompetitionList(response.results);
  }, [fetch]);

  useEffect(() => {
    fetchCompetitionList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Card className={style["competition-card"]}>
      <CardContent className={style["competition-card-content"]}>
        <Typography variant="h4" className={style["competition-card-title"]}>
          Bolões
        </Typography>

        <div>
          {competitionList.map((competition) => (
            <ListCompetitionRow
              key={`competition-item-${competition.id}`}
              competition={competition}
              user={props.user}
            />
          ))}
        </div>
      </CardContent>
    </Card>
  );
};

export default ListCompetitionContainer;
