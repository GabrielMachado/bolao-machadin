import {
  Button,
  Card,
  CardActions,
  CardContent,
  TextField,
  Typography,
} from "@mui/material";
import { useRouter } from "next/router";
import { useCallback, useState } from "react";
import { IUser } from "../../../domain";
import { useFetch } from "../../hooks/useFetch";
import { useLoading } from "../../hooks/useLoading";
import style from "./AuthPages.module.css";

const SignInContainer = () => {
  const [user, setUser] = useState<Pick<IUser, "email" | "password" | "name">>({
    email: "",
    password: "",
    name: "",
  });
  const { loading } = useLoading();
  const { fetch } = useFetch();
  const router = useRouter();

  const setUserProperty = (field: keyof IUser, value: any) => {
    setUser({ ...user, [field]: value });
  };

  const signUp = useCallback(
    async (user: Pick<IUser, "email" | "password" | "name">) => {
      const result = await fetch({
        method: "POST",
        url: `/api/user/create`,
        body: user,
      });

      if (!!result?.id) return router.push("/auth/sign-in");
    },
    [fetch, router]
  );

  return (
    <Card className={style["auth-card-container"]}>
      <CardContent className={style["auth-card"]}>
        <Typography variant="h4" className={style["auth-card-title"]}>
          Cadastro
        </Typography>

        <TextField
          label={"Nome"}
          value={user.name}
          onChange={(e) => setUserProperty("name", e.target.value)}
          type={"text"}
          size={"small"}
          fullWidth={true}
        />

        <TextField
          label={"Email"}
          value={user.email}
          onChange={(e) => setUserProperty("email", e.target.value)}
          type={"email"}
          size={"small"}
          fullWidth={true}
        />

        <TextField
          label={"Password"}
          value={user.password}
          onChange={(e) => setUserProperty("password", e.target.value)}
          type={"password"}
          size={"small"}
          fullWidth={true}
        />
      </CardContent>

      <CardActions style={{ justifyContent: "flex-end" }}>
        <Button
          disabled={!!loading}
          size={"small"}
          onClick={() => signUp(user)}
        >
          Cadastrar
        </Button>
      </CardActions>
    </Card>
  );
};

export default SignInContainer;
