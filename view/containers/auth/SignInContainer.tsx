import {
  Button,
  Card,
  CardActions,
  CardContent,
  TextField,
  Typography,
} from "@mui/material";
import { useRouter } from "next/router";
import { useCallback, useState } from "react";
import { IUser } from "../../../domain";
import { useFetch } from "../../hooks/useFetch";
import { useLoading } from "../../hooks/useLoading";
import style from "./AuthPages.module.css";

const SignInContainer = () => {
  const [user, setUser] = useState<Pick<IUser, "email" | "password">>({
    email: "",
    password: "",
  });
  const { loading } = useLoading();
  const { fetch } = useFetch();
  const router = useRouter();

  const setUserProperty = (field: keyof IUser, value: any) => {
    setUser({ ...user, [field]: value });
  };

  const signIn = useCallback(
    async (user: Pick<IUser, "password" | "email">) => {
      const response: IUser = await fetch({
        method: "POST",
        url: `/api/auth/sign-in`,
        body: user,
      });

      if (response?.id) return router.push(`/competition/list`);
    },
    [fetch, router]
  );

  return (
    <Card className={style["auth-card-container"]}>
      <CardContent className={style["auth-card"]}>
        <Typography variant="h4" className={style["auth-card-title"]}>
          Login
        </Typography>

        <TextField
          label={"Email"}
          value={user.email}
          onChange={(e) => setUserProperty("email", e.target.value)}
          type={"email"}
          size={"small"}
          fullWidth={true}
        />

        <TextField
          label={"Password"}
          value={user.password}
          onChange={(e) => setUserProperty("password", e.target.value)}
          type={"password"}
          size={"small"}
          fullWidth={true}
        />
      </CardContent>

      <CardActions style={{ justifyContent: "flex-end" }}>
        <Button
          disabled={!!loading}
          size={"small"}
          onClick={() => signIn(user)}
        >
          entrar
        </Button>
      </CardActions>
    </Card>
  );
};

export default SignInContainer;
