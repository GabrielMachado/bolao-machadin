import { IUserCompetition, IUserCompetitionBet } from "../../../../domain";
import { useCallback, useEffect, useState } from "react";
import { useFetch } from "../../../hooks/useFetch";
import { ListResponseDTO } from "../../../../domain/dtos/ListDTO";
import style from "./ShowUserCompetitionContainer.module.css";
import { Button } from "@mui/material";
import { useLoading } from "../../../hooks/useLoading";
import qs from "query-string";
import {
  UserCompetitionBetRow,
  UserCompetitionBetToUpdate,
} from "./components/UserCompetitionBetRow";
import { isInteger } from "lodash";

interface ShowUserCompetitionContainerProps {
  userCompetitionId: IUserCompetition["id"];
}

const itensToSaveFilted = (
  userCompetitionBetList: UserCompetitionBetToUpdate[]
) =>
  userCompetitionBetList.filter((userCompetitionBet) => {
    const hasNewHostPredicition =
      userCompetitionBet?.newHostScorePrediction !==
      userCompetitionBet.hostScorePrediction;

    const hasNewVisitorPrediction =
      userCompetitionBet?.newVisitorScorePrediction !==
      userCompetitionBet.visitorScorePrediction;

    return (
      isInteger(userCompetitionBet?.newHostScorePrediction) &&
      isInteger(userCompetitionBet.newVisitorScorePrediction) &&
      (hasNewHostPredicition || hasNewVisitorPrediction)
    );
  });

const ShowUserCompetitionContainer = (
  props: ShowUserCompetitionContainerProps
) => {
  const [userCompetitionBetList, setUserCompetitionBetList] = useState<
    UserCompetitionBetToUpdate[]
  >([]);

  const { fetch } = useFetch();
  const { loading } = useLoading();

  const fetchUserCompetitionBet = useCallback(async () => {
    const query = qs.stringify({
      sortBy: "match.date",
      sortDir: "desc",
    });

    const response: ListResponseDTO<IUserCompetitionBet> = await fetch({
      method: "GET",
      url: `/api/bet/${props.userCompetitionId}/list?${query}`,
    });

    if (!!response?.results)
      setUserCompetitionBetList(
        response.results.map((item) => ({
          ...item,
          newHostScorePrediction: item.hostScorePrediction,
          newVisitorScorePrediction: item.visitorScorePrediction,
        }))
      );
  }, [props.userCompetitionId, fetch]);

  const updateHostScorePrediction = (
    userCompetitionBetList: UserCompetitionBetToUpdate[],
    userCompetitionBetId: IUserCompetitionBet["id"],
    hostScorePrediction?: number
  ) => {
    const userCompetitionBetUpdated = [...userCompetitionBetList];
    const idx = userCompetitionBetUpdated.findIndex(
      (item) => item.id === userCompetitionBetId
    );
    userCompetitionBetUpdated[idx].newHostScorePrediction = hostScorePrediction;
    setUserCompetitionBetList(userCompetitionBetUpdated);
  };

  const updateVisitorScorePrediction = (
    userCompetitionBetList: UserCompetitionBetToUpdate[],
    userCompetitionBetId: IUserCompetitionBet["id"],
    visitorScorePrediction?: number
  ) => {
    const userCompetitionBetUpdated = [...userCompetitionBetList];
    const idx = userCompetitionBetUpdated.findIndex(
      (item) => item.id === userCompetitionBetId
    );
    userCompetitionBetUpdated[idx].newVisitorScorePrediction =
      visitorScorePrediction;

    setUserCompetitionBetList(userCompetitionBetUpdated);
  };

  const savePredictions = useCallback(
    async (userCompetitionBetList: UserCompetitionBetToUpdate[]) => {
      const itensFiltered = itensToSaveFilted(userCompetitionBetList);
      const itensToSave: IUserCompetitionBet[] = itensFiltered.map((item) => ({
        ...item,
        hostScorePrediction: item.newHostScorePrediction,
        visitorScorePrediction: item.newVisitorScorePrediction,
      }));

      await fetch({
        method: "PUT",
        url: `/api/bet/${props.userCompetitionId}/update-many`,
        body: itensToSave,
        successMessage: "Palpites salvos",
      });

      await fetchUserCompetitionBet();
    },

    [fetch, props.userCompetitionId, fetchUserCompetitionBet]
  );

  useEffect(() => {
    fetchUserCompetitionBet();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={style["user-competition-bet-list"]}>
      {userCompetitionBetList.map((item) => (
        <UserCompetitionBetRow
          updateVisitorScorePrediction={(visitorScorePrediction?: number) =>
            updateVisitorScorePrediction(
              userCompetitionBetList,
              item.id,
              visitorScorePrediction
            )
          }
          updateHostScorePrediction={(hostScorePrediction?: number) =>
            updateHostScorePrediction(
              userCompetitionBetList,
              item.id,
              hostScorePrediction
            )
          }
          key={item.id}
          userCompetitionBet={item}
        />
      ))}
      {!!itensToSaveFilted(userCompetitionBetList).length && (
        <Button
          variant={"contained"}
          color={"success"}
          className={style["user-competition-bet-save-button"]}
          onClick={() => savePredictions(userCompetitionBetList)}
          disabled={loading}
        >
          salvar
        </Button>
      )}
    </div>
  );
};

export default ShowUserCompetitionContainer;
