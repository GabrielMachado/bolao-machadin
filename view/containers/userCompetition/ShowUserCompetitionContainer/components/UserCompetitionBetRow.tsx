import Typography from "@mui/material/Typography";
import { parseDatetime } from "../../../../utils/date";
import Image from "next/image";
import { isInteger } from "lodash";
import { IUserCompetitionBet } from "../../../../../domain";

export interface UserCompetitionBetToUpdate extends IUserCompetitionBet {
  newHostScorePrediction: IUserCompetitionBet["hostScorePrediction"];
  newVisitorScorePrediction: IUserCompetitionBet["visitorScorePrediction"];
}
import style from "./UserCompetitionBetRow.module.css";

export const UserCompetitionBetRow = (props: {
  userCompetitionBet: UserCompetitionBetToUpdate;
  updateHostScorePrediction: (hostScorePrediction?: number) => void;
  updateVisitorScorePrediction: (visitorScorePrediction?: number) => void;
}) => {
  const bet = props.userCompetitionBet.bet;
  const betStartDate = new Date(bet.startDate);
  const betEndDate = new Date(bet.endDate);
  const now = new Date();

  const match = props.userCompetitionBet.bet.match;
  const visitor = match.competitionVisitor?.team;
  const host = match.competitionHost?.team;

  const isBetOpened =
    betStartDate.getTime() < now.getTime() &&
    betEndDate.getTime() > now.getTime();

  const statusLabel = isBetOpened ? "Aberto" : "Fechado";

  return (
    <div className={style["user-competition-bet-row-container"]}>
      <Typography variant={"subtitle2"}>
        {`${host?.name} x ${visitor?.name} (${statusLabel})`}
      </Typography>
      <Typography variant={"caption"}>{parseDatetime(match.date)}</Typography>

      <div className={style["user-competition-bet-row-input-container"]}>
        <Image src={host?.logoUrl!} width="24" height="18" alt={host?.name!} />
        <input
          className={style["user-competition-bet-row-input"]}
          type={"number"}
          step="1"
          disabled={!isBetOpened}
          value={
            isInteger(props.userCompetitionBet.newHostScorePrediction)
              ? props.userCompetitionBet.newHostScorePrediction
              : ""
          }
          onChange={(e) =>
            props.updateHostScorePrediction(
              e.target.value ? Number(e.target.value) : undefined
            )
          }
        />
        <input
          className={style["user-competition-bet-row-input"]}
          type={"number"}
          step="1"
          disabled={!isBetOpened}
          value={
            isInteger(props.userCompetitionBet.newVisitorScorePrediction)
              ? props.userCompetitionBet.newVisitorScorePrediction
              : ""
          }
          onChange={(e) =>
            props.updateVisitorScorePrediction(
              e.target.value ? Number(e.target.value) : undefined
            )
          }
        ></input>
        <Image
          src={visitor?.logoUrl!}
          width="24"
          height="18"
          alt={visitor?.name!}
        />
      </div>
    </div>
  );
};
