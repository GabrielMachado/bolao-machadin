import format from "date-fns/format";

export const parseDatetime = (date: Date) => {
  return format(new Date(date), "dd/MM/yyyy - HH:mm");
};

export const parseDate = (date: Date) => {
  return format(new Date(date), "dd/MM/yyyy");
};
