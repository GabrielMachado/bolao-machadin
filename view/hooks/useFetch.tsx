import { createContext, useContext, useState } from "react";
import { useLoading } from "./useLoading";
import { useNotify } from "./useNotify";
import { BaseError } from "../../domain";

interface FetchProviderProps {
  children: JSX.Element;
}

interface FetchParams {
  url: string;
  method: "GET" | "POST" | "DELETE" | "PUT" | "PATCH";
  body?: any;
  successMessage?: string;
  failMessage?: string;
}

interface LoadingContextData {
  fetch: (params: FetchParams) => Promise<any>;
}

const FetchContext = createContext<LoadingContextData>({
  fetch: (params: FetchParams) =>
    Promise.reject(new Error("useFetch not loaded yet")),
});

export const FetchProvider = ({ children }: FetchProviderProps) => {
  const { setLoading } = useLoading();
  const { notify } = useNotify();

  const makeRequest = async (params: FetchParams) => {
    try {
      setLoading(true);
      const response = await fetch(params.url, {
        method: params.method,
        ...(!!params.body && { body: JSON.stringify(params.body) }),
      });

      const body = await response.json();
      if (!response.ok) throw new BaseError(body.message, response.status);
      notify({
        message: params.successMessage || "Sucesso!",
        variant: "success",
      });

      return body;
    } catch (error) {
      if (error instanceof BaseError) {
        notify({
          message: error.message,
          variant: "error",
        });
      } else {
        notify({
          message: params.failMessage || "Tente novamente mais tarde",
          variant: "error",
        });
      }
    } finally {
      setLoading(false);
    }
  };

  return (
    <FetchContext.Provider value={{ fetch: makeRequest }}>
      <>{children}</>
    </FetchContext.Provider>
  );
};

export const useFetch = () => {
  const context = useContext(FetchContext);
  if (!context) {
    throw new Error("useFetch must be used within LoadingProvider");
  }

  return context;
};
