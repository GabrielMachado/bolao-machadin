import { createContext, useContext, useState } from "react";
import { LinearProgress } from "@mui/material";

interface LoadingProviderProps {
  children: JSX.Element;
}

interface LoadingContextData {
  loading: boolean;
  setLoading: (loading: boolean) => void;
}

const LoadingContext = createContext<LoadingContextData>({
  loading: false,
  setLoading: (loading: boolean) => {},
});

export const LoadingProvider = ({ children }: LoadingProviderProps) => {
  const [loading, setLoading] = useState(false);
  const value = { loading, setLoading };

  return (
    <LoadingContext.Provider value={value}>
      <>
        {loading && <LinearProgress className="linear-progress" />}
        {children}
      </>
    </LoadingContext.Provider>
  );
};

export const useLoading = () => {
  const context = useContext(LoadingContext);
  if (!context) {
    throw new Error("useLoading must be used within LoadingProvider");
  }
  return context;
};
