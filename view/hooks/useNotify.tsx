import { createContext, useCallback, useContext, useState } from "react";
import { Alert, AlertColor, Snackbar } from "@mui/material";

interface NotifyProviderProps {
  children: JSX.Element;
}

interface NotifyContextData {
  notify: (params: {
    message: string;
    variant?: AlertColor;
    hideDurantionInMilissecond?: number;
  }) => void;
}

const NotifyContext = createContext<NotifyContextData>({
  notify: (params: {
    message: string;
    variant?: AlertColor;
    hideDurantionInMilissecond?: number;
  }) => {
    throw new Error("Method wass not loaded yey");
  },
});

export const NotifyProvider = ({ children }: NotifyProviderProps) => {
  const [message, setMessage] = useState("");
  const [open, setOpen] = useState(false);
  const [variant, setVariant] = useState<AlertColor>("info");
  const [hideDurantionInMilissecond, setHideDurantion] = useState(5000);

  const notify = (params: {
    message: string;
    variant?: AlertColor;
    hideDurantionInMilissecond?: number;
  }) => {
    const { message, variant, hideDurantionInMilissecond } = params;
    setMessage(message);

    if (!!variant) setVariant(variant);
    if (!!hideDurantionInMilissecond)
      setHideDurantion(hideDurantionInMilissecond);

    setOpen(true);
  };

  const value = { notify };

  return (
    <NotifyContext.Provider value={value}>
      <>
        {children}
        <Snackbar
          open={open}
          autoHideDuration={hideDurantionInMilissecond}
          onClose={() => setOpen(false)}
          anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        >
          <Alert onClose={() => setOpen(false)} severity={variant}>
            {message}
          </Alert>
        </Snackbar>
      </>
    </NotifyContext.Provider>
  );
};

export const useNotify = () => {
  const context = useContext(NotifyContext);
  if (!context) {
    throw new Error("useNotify must be used within LoadingProvider");
  }
  return context;
};
