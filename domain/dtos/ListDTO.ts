export interface ListResponseDTO<T> {
  total: number;
  results: T[];
}

export interface ListRequestDTO {
  page?: number;
  limit?: number;

  sortBy?: string;
  sortDir?: string;

  [key: string]: any;
}
