import { BaseEntity } from "./BaseEntity";
import { ITeam } from "./Team";
import { ICompetition } from "./Competition";
import { IMatch } from "./Match";

export interface ICompetitionTeam extends BaseEntity {
  competitionId: ICompetition["id"];
  teamId: ITeam["id"];

  team: ITeam;
  competition: ICompetition;
  matches: IMatch[];
}
