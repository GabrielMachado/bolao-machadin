import { BaseEntity } from "./BaseEntity";
import { ICompetition } from "./Competition";
import { IUser } from "./User";

export interface IUserCompetition extends BaseEntity {
  userId: IUser["id"];
  competitionId: ICompetition["id"];
  status: boolean;
  totalPoints: number;

  user: IUser;
  competition: ICompetition;
}
