import { BaseEntity } from "./BaseEntity";
import { ICompetitionTeam } from "./CompetitionTeam";
import { IMatch } from "./Match";
import { IUserCompetition } from "./UserCompetition";

export interface ICompetition extends BaseEntity {
  name: string;
  startDate: Date;
  endDate: Date;

  userCompetitionList?: IUserCompetition[];
  teams?: ICompetitionTeam[];
  matches?: IMatch[];
}
