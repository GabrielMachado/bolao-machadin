import { BaseEntity } from "./BaseEntity";
import { ICompetition } from "./Competition";
import { ICompetitionTeam } from "./CompetitionTeam";
import { IUserCompetitionBet } from "./UserCompetitionBet";

export interface IMatch extends BaseEntity {
  hostId: ICompetitionTeam["id"];
  visitorId: ICompetitionTeam["id"];
  date: Date;
  hostScore: number;
  visitorScore: number;
  competitionId: ICompetition["id"];

  competitionHost?: ICompetitionTeam;
  competitionVisitor?: ICompetitionTeam;
  userCompetitionBetList?: IUserCompetitionBet[];
  competition?: ICompetition;
}
