import { BaseEntity } from "./BaseEntity";
import { ICompetitionTeam } from "./CompetitionTeam";

export interface ITeam extends BaseEntity {
  name: string;
  logoUrl: string;

  competitions: ICompetitionTeam;
}
