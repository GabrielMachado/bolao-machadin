import { BaseEntity } from "./BaseEntity";
import { IUserCompetition } from "./UserCompetition";

export enum UserClaim {
  ADMIN = 100,
  USER = 1,
}

export interface IUser extends BaseEntity {
  name: string;
  email: string;
  password: string;
  claim: UserClaim;

  competitions: IUserCompetition[];
}
