import { BaseEntity } from "./BaseEntity";
import { IBet } from "./Bet";
import { IUserCompetition } from "./UserCompetition";

export interface IUserCompetitionBet extends BaseEntity {
  hostScorePrediction?: number;
  visitorScorePrediction?: number;
  points: number;
  betId: IBet["id"];
  userCompetitionId: IUserCompetition["id"];

  userCompetition: IUserCompetition;
  bet: IBet;
}
