import { BaseEntity } from "./BaseEntity";
import { IMatch } from "./Match";
import { IUserCompetitionBet } from "./UserCompetitionBet";

export interface IBet extends BaseEntity {
  startDate: Date;
  endDate: Date;

  match: IMatch;
  users: IUserCompetitionBet[];
}
