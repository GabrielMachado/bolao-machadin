# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] 2022-10-29

### Added

- Competition settings page
- User bet competition report

### Changed

- Use contained button instead of text button to flag user competition status

### Fixed

- Set viewport metatag to avoid unecessary zoom in safari
- Fixed header and scroll

## [0.1.0] 2022-10-23

### Added

- Screen to sign in
- Screen to sign up
- Screen to list competitions
- Join in a competition
- Screen to list user competition bets
- Update many user competition bets
- Endpoint to update a match result
