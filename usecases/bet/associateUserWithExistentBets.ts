import Bet from "../../database/models/Bet";
import Match from "../../database/models/Match";
import UserCompetition from "../../database/models/UserCompetition";
import UserCompetitionBet from "../../database/models/UserCompetitionBet";
import { IUserCompetition, IUserCompetitionBet } from "../../domain";
import { logger } from "../../utils/logger";

const context = "associate User With Existent Bets";

const associateUserWithExistentBets = async (
  userCompetitionId: IUserCompetition["id"],
  trx?: any
): Promise<IUserCompetitionBet[]> => {
  if (!trx) return associateUserWithExistentBets(userCompetitionId, trx);

  logger.info({
    context,
    message: `Will associate user Competition ${userCompetitionId} with existent bets`,
  });

  const userCompetition = await UserCompetition.query(trx).findById(
    userCompetitionId
  );

  const bets = await Bet.query(trx)
    .leftJoin(Match.tableName, `${Match.tableName}.id`, `${Bet.tableName}.id`)
    .where({
      "match.competitionId": userCompetition?.competitionId,
    });

  logger.info({
    context,
    message: `${bets.length} found`,
  });

  const userBet: Partial<IUserCompetitionBet>[] = bets.map((bet) => ({
    points: 0,
    userCompetitionId: userCompetition?.id,
    betId: bet.id,
  }));

  return UserCompetitionBet.query(trx).insert(userBet);
};

export default associateUserWithExistentBets;
