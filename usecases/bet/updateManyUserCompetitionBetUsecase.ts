import UserCompetition from "../../database/models/UserCompetition";
import UserCompetitionBet from "../../database/models/UserCompetitionBet";
import { IUser, IUserCompetitionBet } from "../../domain";
import updateOneUserCompetitionByIdUsecase from "./updateOneUserCompetitionBetUsecase";

export const updateManyUserCompetitionBetUsecase = async (
  userCompetitionId: IUserCompetitionBet["id"],
  userCompetitionBetList: IUserCompetitionBet[],
  user: IUser,
  trx?: any
): Promise<IUserCompetitionBet[]> => {
  if (!trx)
    return UserCompetitionBet.transaction((trx) =>
      updateManyUserCompetitionBetUsecase(
        userCompetitionId,
        userCompetitionBetList,
        user,
        trx
      )
    );

  const currentUserCompetitionBetList = await UserCompetitionBet.query(trx)
    .join(
      `${UserCompetition.tableName} as userCompetition`,
      `userCompetition.id`,
      `${UserCompetitionBet.tableName}.userCompetitionId`
    )
    .whereIn(
      `${UserCompetitionBet.tableName}.id`,
      userCompetitionBetList.map((item) => item.id)
    )
    .andWhere({
      userCompetitionId,
      userId: user.id,
    });

  const result = await Promise.all(
    currentUserCompetitionBetList.map(async (currentUserCompetitionBet) => {
      const userCompetitionBetToUpdate = userCompetitionBetList.find(
        (toUpdate) =>
          Number(toUpdate.id) === Number(currentUserCompetitionBet.id)
      );
      return updateOneUserCompetitionByIdUsecase(
        {
          id: currentUserCompetitionBet.id,
          betId: currentUserCompetitionBet.betId,
          hostScorePrediction: userCompetitionBetToUpdate?.hostScorePrediction!,
          visitorScorePrediction:
            userCompetitionBetToUpdate?.visitorScorePrediction!,
        },
        trx
      );
    })
  );

  return result;
};
