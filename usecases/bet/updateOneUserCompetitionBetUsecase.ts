import Bet from "../../database/models/Bet";
import UserCompetitionBet from "../../database/models/UserCompetitionBet";
import { IBet, IUserCompetitionBet } from "../../domain";
import { BadRequestError } from "../../domain/errors/BadRequestError";

const updateOneUserCompetitionByIdUsecase = async (
  userCompetitionBet: Pick<
    IUserCompetitionBet,
    "id" | "hostScorePrediction" | "visitorScorePrediction" | "betId"
  >,
  trx?: any
) => {
  const bet: IBet | undefined = await Bet.query(trx).findById(
    userCompetitionBet.betId
  );

  if (new Date(bet?.startDate!).getTime() > new Date().getTime())
    throw new BadRequestError(`Bet ${bet?.id} is not opened yet`);

  if (new Date(bet?.endDate!).getTime() < new Date().getTime())
    throw new BadRequestError(`Bet ${bet?.id} is closed`);

  const userCompetitionBetUpdated = await UserCompetitionBet.query(
    trx
  ).updateAndFetchById(userCompetitionBet.id, {
    hostScorePrediction: userCompetitionBet.hostScorePrediction,
    visitorScorePrediction: userCompetitionBet.visitorScorePrediction,
  });

  return userCompetitionBetUpdated;
};

export default updateOneUserCompetitionByIdUsecase;
