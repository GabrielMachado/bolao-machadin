import { isNumber } from "lodash";
import UserCompetition from "../../database/models/UserCompetition";
import UserCompetitionBet from "../../database/models/UserCompetitionBet";
import { IMatch, IUserCompetitionBet } from "../../domain";

const resolvePoints = (
  match: IMatch,
  userCompetitionBet: IUserCompetitionBet
) => {
  const { visitorScore, hostScore } = match;
  const { hostScorePrediction, visitorScorePrediction } = userCompetitionBet;

  if (!isNumber(hostScorePrediction) || !isNumber(visitorScorePrediction))
    return 0;

  if (
    hostScorePrediction === hostScore &&
    visitorScorePrediction === visitorScore
  )
    return 4;

  if (hostScore > visitorScore && hostScorePrediction > visitorScorePrediction)
    return 2;

  if (hostScore < visitorScore && hostScorePrediction < visitorScorePrediction)
    return 2;

  if (
    hostScore === visitorScore &&
    hostScorePrediction === visitorScorePrediction
  )
    return 2;

  return 0;
};

const consolidateOneUserCompetitionBetUsecase = async (
  match: IMatch,
  userCompetitionBet: IUserCompetitionBet,
  trx?: any
) => {
  const points = resolvePoints(match, userCompetitionBet);

  await UserCompetition.query(trx).updateAndFetchById(
    userCompetitionBet.userCompetition.id,
    { totalPoints: userCompetitionBet.userCompetition.totalPoints + points }
  );

  const userCompetition = await UserCompetitionBet.query(
    trx
  ).updateAndFetchById(userCompetitionBet.id, { points });

  return userCompetition;
};

export default consolidateOneUserCompetitionBetUsecase;
