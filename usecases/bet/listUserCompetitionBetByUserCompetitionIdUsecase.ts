import applyListRequestToQuery from "../../database/applyListRequestToQuery";
import Bet from "../../database/models/Bet";
import Match from "../../database/models/Match";
import UserCompetition from "../../database/models/UserCompetition";
import UserCompetitionBet from "../../database/models/UserCompetitionBet";
import { IUser, IUserCompetitionBet } from "../../domain";
import { ListRequestDTO } from "../../domain/dtos/ListDTO";

const listUserCompetitionBetByUserCompetitionIdUsecase = async (
  userCompetitionId: IUserCompetitionBet["id"],
  params: ListRequestDTO,
  user: IUser,
  trx?: any
) => {
  const query = UserCompetitionBet.query(trx)
    .join(
      UserCompetition.tableName,
      `${UserCompetition.tableName}.id`,
      `${UserCompetitionBet.tableName}.userCompetitionId`
    )
    .join(
      Bet.tableName,
      `${Bet.tableName}.id`,
      `${UserCompetitionBet.tableName}.betId`
    )
    .join(Match.tableName, `${Bet.tableName}.id`, `${Match.tableName}.id`)
    .where({ userCompetitionId, userId: user.id })
    .withGraphFetched(
      "[bet.[match.[competitionVisitor.[team], competitionHost.[team], competition]]]"
    );
  const result = await applyListRequestToQuery<IUserCompetitionBet>(
    params,
    query
  );
  return result;
};

export default listUserCompetitionBetByUserCompetitionIdUsecase;
