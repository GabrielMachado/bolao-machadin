import Match from "../../database/models/Match";
import UserCompetitionBet from "../../database/models/UserCompetitionBet";
import { IMatch, IUserCompetitionBet } from "../../domain";
import consolidateOneUserCompetitionBetUsecase from "../bet/consolidateOneUserCompetitionBetUsecase";

const finishedMatchByIdUsecase = async (
  id: IMatch["id"],
  matchResult: Pick<IMatch, "hostScore" | "visitorScore">,
  trx?: any
): Promise<IMatch> => {
  if (!trx)
    return Match.transaction((trx) =>
      finishedMatchByIdUsecase(id, matchResult, trx)
    );

  const userCompetitionBetList = await UserCompetitionBet.query(trx)
    .where({
      betId: id,
    })
    .withGraphFetched("[userCompetition]");

  const match = await Match.query(trx).updateAndFetchById(id, {
    hostScore: matchResult.hostScore,
    visitorScore: matchResult.visitorScore,
  });

  const consolidatedUserCompetitionBetList = await Promise.all(
    userCompetitionBetList.map(
      async (userCompetitionBet: IUserCompetitionBet) =>
        consolidateOneUserCompetitionBetUsecase(match, userCompetitionBet, trx)
    )
  );

  return {
    ...match,
    userCompetitionBetList: consolidatedUserCompetitionBetList,
  };
};

export default finishedMatchByIdUsecase;
