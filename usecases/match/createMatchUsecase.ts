import { omit } from "lodash";
import Bet from "../../database/models/Bet";
import Match from "../../database/models/Match";
import UserCompetition from "../../database/models/UserCompetition";
import UserCompetitionBet from "../../database/models/UserCompetitionBet";
import { IBet, IMatch } from "../../domain";

type CreateMatchUsecaseDto = Pick<
  IMatch,
  "hostId" | "visitorId" | "date" | "competitionId"
> &
  Pick<IBet, "endDate">;

const createMatchUsecase = async (
  params: CreateMatchUsecaseDto,
  trx?: any
): Promise<IMatch> => {
  if (!trx) return Match.transaction((trx) => createMatchUsecase(params, trx));
  const match = await Match.query(trx).insertAndFetch(omit(params, "endDate"));
  const bet = await Bet.query(trx).insertAndFetch({
    id: match.id,
    startDate: new Date(),
    endDate: new Date(params.endDate),
  });
  const userCompetitionList = await UserCompetition.query(trx).where({
    competitionId: params.competitionId,
  });
  const userCompetitionBetListToInsert = userCompetitionList.map((item) => ({
    betId: bet.id,
    userCompetitionId: item.id,
  }));

  const userCompetitionBetList = await Promise.all(
    userCompetitionBetListToInsert.map((item) =>
      UserCompetitionBet.query(trx).insertAndFetch(item)
    )
  );

  return {
    ...match,
    userCompetitionBetList,
  };
};

export default createMatchUsecase;
