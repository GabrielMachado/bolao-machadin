import json2csv from "json2csv";
import UserCompetition from "../../database/models/UserCompetition";
import { ICompetition } from "../../domain";

const generateUserCompetitionBetUsecase = async (
  competitionId: ICompetition["id"]
) => {
  const knex = UserCompetition.knex();

  const query = `select
      match.date date,
      host.name mandante,
      visitor.name visitante,
      match."hostScore" placar_mandante,
      match."visitorScore" placar_visitante,
      "user".email,
      user_competition_bet.points pontos_obtidos,
      user_competition_bet."hostScorePrediction" previsao_mandante,
      user_competition_bet."visitorScorePrediction" previsao_visitante
  from
  user_competition_bet
  join user_competition on user_competition.id = user_competition_bet."userCompetitionId"
  join "user" on "user".id = user_competition."userId"
  join bet on bet.id = user_competition_bet."betId"
  join match on match.id = bet.id
  join competition_team hostInCompetition on hostInCompetition.id = match."hostId"
  join competition_team visitorInCompetition on visitorInCompetition.id = match."visitorId"
  join team host on host.id = hostInCompetition."teamId"
  join team visitor on visitor.id = visitorInCompetition."teamId"
  join competition on competition.id = user_competition."competitionId"
  where
      competition.id=${competitionId}
  order by
      match.id asc,
      "user".email asc`;

  const { rows } = await knex.raw(query);
  const result = json2csv.parse(rows || []);
  return {
    report: result,
  };
};

export default generateUserCompetitionBetUsecase;
