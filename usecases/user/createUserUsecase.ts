import User from "../../database/models/User";
import { IUser, UserClaim } from "../../domain";
import bcrypt from "bcrypt";
import { ConflictError } from "../../domain/errors/ConflictError";
import { logger } from "../../utils/logger";
import omit from "lodash/omit";

const createUserUsecase = async (
  user: Pick<IUser, "name" | "email" | "password">,
  trx?: any
) => {
  logger.info({
    context: "create user usecase",
    message: "User being creating",
    details: {
      ...omit(user, "password"),
    },
  });

  const existentEmail = await User.query(trx).findOne({ email: user.email });
  if (!!existentEmail) {
    throw new ConflictError("Email already associeted with another user");
  }

  const password = await bcrypt.hash(user.password, 10);
  const userCreated = await User.query(trx).insertAndFetch({
    ...user,
    password: password,
    claim: UserClaim.USER,
  });

  return userCreated;
};

export default createUserUsecase;
