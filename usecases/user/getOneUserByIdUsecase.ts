import User from "../../database/models/User";
import { IUser, NotFoundError } from "../../domain";

const getOneUserByIdUsecase = async (id: IUser["id"], trx?: any) => {
  const user = await User.query(trx).findById(id);
  if (!user) {
    throw new NotFoundError(`User with id ${id} not found`);
  }

  return user;
};

export default getOneUserByIdUsecase;
