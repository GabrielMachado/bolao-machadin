import User from "../../database/models/User";
import { IUser, NotFoundError } from "../../domain";

const getOneUserByEmailUsecase = async (email: IUser["email"], trx?: any) => {
  const user = await User.query(trx).findOne({ email });
  if (!user) {
    throw new NotFoundError(`User with email ${email} not found`);
  }

  return user;
};

export default getOneUserByEmailUsecase;
