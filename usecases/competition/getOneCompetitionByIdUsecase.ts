import Competition from "../../database/models/Competition";
import { NotFoundError } from "../../domain";

const getOneCompetitionByIdUsecase = async (
  id: Competition["id"],
  trx?: any
) => {
  const competition = await Competition.query(trx).findById(id);
  if (!competition)
    throw new NotFoundError(`Competition with id ${id} not found`);

  return competition;
};

export default getOneCompetitionByIdUsecase;
