import Competition from "../../database/models/Competition";
import { ListRequestDTO } from "../../domain/dtos/ListDTO";
import applyListRequestToQuery from "../../database/applyListRequestToQuery";
import { ICompetition, IUser } from "../../domain";

const listCompetitionUsecase = async (
  params: ListRequestDTO,
  user: IUser,
  trx?: any
) => {
  const query = Competition.query(trx)
    .withGraphFetched("[userCompetitionList(subscribedCompetition)]")
    .modifiers({
      subscribedCompetition(builder) {
        builder.where({ userId: user.id });
      },
    });
  const result = await applyListRequestToQuery<ICompetition>(params, query);
  return result;
};

export default listCompetitionUsecase;
