import UserCompetition from "../../database/models/UserCompetition";
import {
  ConflictError,
  ICompetition,
  IUser,
  IUserCompetition,
} from "../../domain";
import { logger } from "../../utils/logger";
import associateUserWithExistentBets from "../bet/associateUserWithExistentBets";
import getOneUserByIdUsecase from "../user/getOneUserByIdUsecase";
import getOneCompetitionByIdUsecase from "./getOneCompetitionByIdUsecase";

const context = "join Competition Usecase";

export const joinCompetitionUsecase = async (
  userId: IUser["id"],
  competitionId: ICompetition["id"],
  trx?: any
): Promise<IUserCompetition> => {
  if (!trx)
    return UserCompetition.transaction((trx) =>
      joinCompetitionUsecase(userId, competitionId, trx)
    );

  logger.info({
    context,
    message: `User ${userId} is trying to join in competition ${competitionId}`,
  });

  const competition = await getOneCompetitionByIdUsecase(competitionId, trx);
  const user = await getOneUserByIdUsecase(userId, trx);

  const existentUserCompetition = await UserCompetition.query(trx).findOne({
    competitionId: competition.id,
    userId: user.id,
  });

  if (!!existentUserCompetition)
    throw new ConflictError(
      `User ${user.email} already has joined in competition ${competition.name}`
    );

  const userCompetition: Partial<IUserCompetition> = {
    competitionId: competition.id,
    userId,
    totalPoints: 0,
    status: false,
  };

  const userCompetitionCreated = await UserCompetition.query(
    trx
  ).insertAndFetch(userCompetition);

  logger.info({
    context,
    message: `User ${userId} joined in competition ${competitionId}`,
  });

  await associateUserWithExistentBets(userCompetitionCreated.id, trx);

  return userCompetitionCreated;
};
