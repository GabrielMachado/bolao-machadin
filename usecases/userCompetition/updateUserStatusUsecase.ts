import UserCompetition from "../../database/models/UserCompetition";
import { IUserCompetition } from "../../domain";
import getOneUserByIdUsecase from "../user/getOneUserByIdUsecase";

const updateUserCompetitionStatusUsecase = async (
  userCompetitionId: IUserCompetition["id"],
  status: IUserCompetition["status"],
  trx?: any
) => {
  const user = await getOneUserByIdUsecase(userCompetitionId, trx);

  const userUpdated = await UserCompetition.query().updateAndFetchById(
    user.id,
    { status }
  );

  return userUpdated;
};

export default updateUserCompetitionStatusUsecase;
