import applyListRequestToQuery from "../../database/applyListRequestToQuery";
import Competition from "../../database/models/Competition";
import User from "../../database/models/User";
import UserCompetition from "../../database/models/UserCompetition";
import { IUser, IUserCompetition } from "../../domain";
import { ListRequestDTO } from "../../domain/dtos/ListDTO";

const listUserCompetitionUsecase = async (
  params: ListRequestDTO,
  user: IUser,
  trx?: any
) => {
  const query = UserCompetition.query(trx)
    .leftJoin(
      User.tableName,
      `${User.tableName}.id`,
      `${UserCompetition.tableName}.userId`
    )
    .leftJoin(
      Competition.tableName,
      `${Competition.tableName}.id`,
      `${UserCompetition.tableName}.competitionId`
    )
    .withGraphFetched("[user]");

  const result = applyListRequestToQuery<IUserCompetition>(params, query);
  return result;
};

export default listUserCompetitionUsecase;
