import Objection, { OrderByDirection } from "objection";
import isInteger from "lodash/isInteger";
import { ListRequestDTO, ListResponseDTO } from "../domain/dtos/ListDTO";

const applyFilter = (
  filter: ListRequestDTO,
  query: Objection.QueryBuilder<any, any[]>
) => {
  Object.entries(filter).map(([key, value]) => {
    const isArray = Array.isArray(value);

    if (!isArray) {
      if (key.endsWith("_le")) {
        query.where(key.split("_le")[0], "<=", value);
      } else if (key.endsWith("_lt")) {
        query.where(key.split("_lt")[0], "<", value);
      } else if (key.endsWith("_ge")) {
        query.where(key.split("_ge")[0], ">=", value);
      } else if (key.endsWith("_gt")) {
        query.where(key.split("_gt")[0], ">", value);
      } else if (key.endsWith("_includes")) {
        query.where(key.split("_includes")[0], "ilike", "%" + value + "%");
      } else {
        query.where(key, value);
      }
    } else {
      query.whereIn(key, value);
    }
  });
};

const applyListRequestToQuery = async <T>(
  params: ListRequestDTO,
  query: Objection.QueryBuilder<any, any[]>
): Promise<ListResponseDTO<T>> => {
  const { limit, page, sortBy, sortDir, ...filter } = params;

  applyFilter(filter, query);

  if (!!sortBy && sortDir) {
    query.orderBy(params.sortBy!, params.sortDir as OrderByDirection);
  }

  if (isInteger(page) && isInteger(limit)) {
    return query.page(page!, limit!);
  }

  const results = await query;

  return {
    results,
    total: results.length,
  };
};

export default applyListRequestToQuery;
