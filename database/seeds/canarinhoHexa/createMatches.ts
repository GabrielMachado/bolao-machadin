import { Knex } from "knex";
import { ICompetition, ICompetitionTeam, IMatch, ITeam } from "../../../domain";
import { competition } from "./insertCompetition";
import { teamList as teams } from "./insertTeams";

const matchToCreateList = [
  "Qatar,ecuador,2022-11-20T16:00:00.000Z",
  "inglaterra,iran,2022-11-21T13:00:00.000Z",
  "argentina,ksa,2022-11-22T10:00:00.000Z",
  "franca,australia,2022-11-22T19:00:00.000Z",
  "alemanha,japao,2022-11-23T13:00:00.000Z",
  "espanha,costa rica,2022-11-23T16:00:00.000Z",
  "portugal,gana,2022-11-24T16:00:00.000Z",
  "brasil,servia,2022-11-24T19:00:00.000Z",
  "Inglaterra,USA,2022-11-25T19:00:00.000Z",
  "Argentina,Mexico,2022-11-26T19:00:00.000Z",
  "Espanha,Alemanha,2022-11-27T19:00:00.000Z",
  "Brasil,Suica,2022-11-28T16:00:00.000Z",
  "Portugal,Uruguai,2022-11-28T19:00:00.000Z",
  "ecuador,senegal,2022-11-29T15:00:00.000Z",
  "iran,USA,2022-11-29T19:00:00.000Z",
  "País de Gales,inglaterra,2022-11-29T19:00:00.000Z",
  "tunisia,franca,2022-11-30T15:00:00.000Z",
  "polonia,argentina,2022-11-30T19:00:00.000Z",
  "costa rica,alemanha,2022-12-01T19:00:00.000Z",
  "croacia,belgica,2022-12-01T19:00:00.000Z",
  "coreia do sul,portugal,2022-12-02T15:00:00.000Z",
  "gana,uruguai,2022-12-02T15:00:00.000Z",
  "camaroes,brasil,2022-12-02T19:00:00.000Z",
];

export const createMatches = async (knex: Knex) => {
  const competitionList: ICompetition[] = await knex("competition").where({
    name: competition.name,
  });

  const teamList: ITeam[] = await knex("team").whereIn(
    "name",
    teams.map((item) => item.name)
  );

  const competitionTeamList: Pick<
    ICompetitionTeam,
    "competitionId" | "teamId" | "id"
  >[] = await knex("competition_team")
    .select()
    .where({
      competitionId: competitionList[0].id,
    })
    .whereIn(
      "teamId",
      teamList.map((item) => item.id)
    );

  const matchList: Partial<IMatch>[] = matchToCreateList.map(
    (matchToCreate) => {
      const [hostName, visitorName, date] = matchToCreate.split(",");

      const host = teamList.find(
        (team) => team.name.toLocaleLowerCase() === hostName.toLocaleLowerCase()
      );
      if (!host)
        throw new Error(
          `Could not find host ${hostName} in line ${matchToCreate}`
        );

      const hostInCompetition = competitionTeamList.find(
        (competitionTeam) => competitionTeam.teamId === host?.id
      );

      const visitor = teamList.find(
        (team) =>
          team.name.toLocaleLowerCase() === visitorName.toLocaleLowerCase()
      );
      if (!visitor)
        throw new Error(
          `Could not find visitor ${visitorName} in line ${matchToCreate}`
        );

      const visitorInCompetition = competitionTeamList.find(
        (competitionTeam) => competitionTeam.teamId === visitor?.id
      );

      const match: Partial<IMatch> = {
        date: new Date(date),
        visitorId: visitorInCompetition?.id,
        hostId: hostInCompetition?.id,
        competitionId: hostInCompetition?.competitionId,
      };

      return match;
    }
  );

  await knex("match").insert(matchList);
};
