import { Knex } from "knex";
import { IBet, ICompetition } from "../../../domain";
import { competition } from "./insertCompetition";

export const createBets = async (knex: Knex) => {
  const competitionList: ICompetition[] = await knex("competition").where({
    name: competition.name,
  });
  const competitionFound = competitionList[0];

  const query = `
    select
        match.id id,
        host.name hostName,
        visitor.name visitorName,
        match.date date

    from match
    join competition_team hostInCompetition on hostInCompetition.id = match."hostId"
    join competition_team visitorInCompetition on visitorInCompetition.id = match."visitorId"
    join team host on host.id = hostInCompetition."teamId"
    join team visitor on visitor.id = visitorInCompetition."teamId"
    where
        hostInCompetition."competitionId" = ${competitionFound.id} AND
        visitorInCompetition."competitionId" = ${competitionFound.id}
  `;
  const { rows } = await knex.raw(query);

  const betList: IBet[] = rows.map((match: any) => ({
    startDate: new Date("2022-12-01T03:00:00.000Z"),
    endDate: new Date("2022-12-20T03:00:00.000Z"),
    id: match.id,
  }));

  await knex("bet").insert(betList);
};
