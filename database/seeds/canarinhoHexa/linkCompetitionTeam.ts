import { Knex } from "knex";
import { ICompetition, ITeam } from "../../../domain";
import { competition } from "./insertCompetition";
import { teamList as teams } from "./insertTeams";

export const linkCompetitionTeam = async (knex: Knex) => {
  const competitionList: ICompetition[] = await knex("competition").where({
    name: competition.name,
  });

  const teamList: ITeam[] = await knex("team").whereIn(
    "name",
    teams.map((item) => item.name)
  );

  await knex("competition_team").insert(
    teamList.map((item) => ({
      competitionId: competitionList[0].id,
      teamId: item.id,
    }))
  );
};
