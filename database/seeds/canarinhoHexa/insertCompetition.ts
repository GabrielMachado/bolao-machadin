import { Knex } from "knex";
import { ICompetition } from "../../../domain";

export const competition: Partial<ICompetition> = {
  name: "Canarinho Hexa",
  startDate: new Date("2022-11-20T13:00:00.000Z"),
  endDate: new Date("2022-12-18T18:00:00.000Z"),
};

export const insertCompetition = async (knex: Knex) => {
  await knex("competition").insert(competition);
};
