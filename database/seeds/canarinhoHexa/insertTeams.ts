import { Knex } from "knex";
import { ITeam } from "../../../domain";

export const teamList: Pick<ITeam, "logoUrl" | "name">[] = [
  { name: "Alemanha", logoUrl: "https://flagcdn.com/24x18/de.png" },
  { name: "Argentina", logoUrl: "https://flagcdn.com/24x18/ar.png" },
  { name: "Australia", logoUrl: "https://flagcdn.com/24x18/au.png" },
  { name: "Belgica", logoUrl: "https://flagcdn.com/24x18/be.png" },
  { name: "Brasil", logoUrl: "https://flagcdn.com/24x18/br.png" },
  { name: "Camaroes", logoUrl: "https://flagcdn.com/24x18/cm.png" },
  { name: "Canada", logoUrl: "https://flagcdn.com/24x18/ca.png" },
  { name: "Coreia do Sul", logoUrl: "https://flagcdn.com/24x18/kr.png" },
  { name: "Costa Rica", logoUrl: "https://flagcdn.com/24x18/cr.png" },
  { name: "Croacia", logoUrl: "https://flagcdn.com/24x18/hr.png" },
  { name: "Dinamarca", logoUrl: "https://flagcdn.com/24x18/dk.png" },
  { name: "Ecuador", logoUrl: "https://flagcdn.com/24x18/ec.png" },
  { name: "Espanha", logoUrl: "https://flagcdn.com/24x18/es.png" },
  { name: "Franca", logoUrl: "https://flagcdn.com/24x18/fr.png" },
  { name: "Gana", logoUrl: "https://flagcdn.com/24x18/gh.png" },
  { name: "Inglaterra", logoUrl: "https://flagcdn.com/24x18/gb-eng.png" },
  { name: "Iran", logoUrl: "https://flagcdn.com/24x18/ir.png" },
  { name: "Japao", logoUrl: "https://flagcdn.com/24x18/jp.png" },
  { name: "KSA", logoUrl: "https://flagcdn.com/24x18/sa.png" },
  { name: "Marrocos", logoUrl: "https://flagcdn.com/24x18/ma.png" },
  { name: "Mexico", logoUrl: "https://flagcdn.com/24x18/mx.png" },
  { name: "País de Gales", logoUrl: "https://flagcdn.com/24x18/gb-wls.png" },
  { name: "Países Baixos", logoUrl: "https://flagcdn.com/24x18/nl.png" },
  { name: "Polonia", logoUrl: "https://flagcdn.com/24x18/pl.png" },
  { name: "Portugal", logoUrl: "https://flagcdn.com/24x18/pt.png" },
  { name: "Qatar", logoUrl: "https://flagcdn.com/24x18/qa.png" },
  { name: "Senegal", logoUrl: "https://flagcdn.com/24x18/sn.png" },
  { name: "Servia", logoUrl: "https://flagcdn.com/24x18/rs.png" },
  { name: "Suica", logoUrl: "https://flagcdn.com/24x18/se.png" },
  { name: "Tunisia", logoUrl: "https://flagcdn.com/24x18/tn.png" },
  { name: "USA", logoUrl: "https://flagcdn.com/24x18/us.png" },
  { name: "Uruguai", logoUrl: "https://flagcdn.com/24x18/uy.png" },
];

export const insertTeams = async (knex: Knex) => {
  await knex("team").insert(teamList);
};
