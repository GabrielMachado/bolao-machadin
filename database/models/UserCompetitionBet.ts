import { IUserCompetitionBet } from "../../domain";
import BaseModel from "./BaseModel";
import Bet from "./Bet";
import UserCompetition from "./UserCompetition";

interface UserCompetitionBet extends IUserCompetitionBet {}

class UserCompetitionBet extends BaseModel {
  static tableName = "user_competition_bet";

  static get relationMappings() {
    return {
      bet: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: Bet,
        join: {
          from: "user_competition_bet.betId",
          to: "bet.id",
        },
      },
      userCompetition: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: UserCompetition,
        join: {
          from: "user_competition_bet.userCompetitionId",
          to: "user_competition.id",
        },
      },
    };
  }
}

export default UserCompetitionBet;
