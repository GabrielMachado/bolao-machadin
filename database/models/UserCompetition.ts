import { IUserCompetition } from "../../domain";
import BaseModel from "./BaseModel";
import Competition from "./Competition";
import User from "./User";

interface UserCompetition extends IUserCompetition {}

class UserCompetition extends BaseModel {
  static tableName = "user_competition";

  static get relationMappings() {
    return {
      user: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: "user_competition.userId",
          to: "user.id",
        },
      },
      competition: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: Competition,
        join: {
          from: "user_competition.competitionId",
          to: "competition.id",
        },
      },
    };
  }
}

export default UserCompetition;
