import { ICompetition, ICompetitionTeam } from "../../domain";
import BaseModel from "./BaseModel";
import Competition from "./Competition";
import Team from "./Team";

interface CompetitionTeam extends ICompetitionTeam {}

class CompetitionTeam extends BaseModel {
  static tableName = "competition_team";

  static get relationMappings() {
    return {
      competition: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: Competition,
        join: {
          from: "competition_team.competitionId",
          to: "competition.id",
        },
      },
      team: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: Team,
        join: {
          from: "competition_team.teamId",
          to: "team.id",
        },
      },
      matches: {
        relation: BaseModel.HasManyRelation,
        modelClass: Team,
        join: {
          from: "competition_team.id",
          to: "match.competitionTeamId",
        },
      },
    };
  }
}

export default CompetitionTeam;
