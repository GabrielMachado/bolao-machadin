import { IMatch } from "../../domain";
import BaseModel from "./BaseModel";
import Competition from "./Competition";
import CompetitionTeam from "./CompetitionTeam";

interface Match extends IMatch {}

class Match extends BaseModel {
  static tableName = "match";

  static get relationMappings() {
    return {
      competitionVisitor: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: CompetitionTeam,
        join: {
          from: "match.visitorId",
          to: "competition_team.id",
        },
      },
      competitionHost: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: CompetitionTeam,
        join: {
          from: "match.hostId",
          to: "competition_team.id",
        },
      },
      competition: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: Competition,
        join: {
          from: "match.competitionId",
          to: "competition.id",
        },
      },
    };
  }
}

export default Match;
