import { IBet } from "../../domain";
import BaseModel from "./BaseModel";
import Match from "./Match";

interface Bet extends IBet {}

class Bet extends BaseModel {
  static tableName = "bet";

  static get relationMappings() {
    return {
      match: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: Match,
        join: {
          from: "bet.id",
          to: "match.id",
        },
      },
    };
  }
}

export default Bet;
