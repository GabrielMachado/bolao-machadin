import { IUser } from "../../domain";
import BaseModel from "./BaseModel";
import UserCompetition from "./UserCompetition";

interface User extends IUser {}

class User extends BaseModel {
  static tableName = "user";

  static get relationMappings() {
    return {
      competitions: {
        relation: BaseModel.HasManyRelation,
        modelClass: UserCompetition,
        join: {
          from: "user.id",
          to: "user_competition.userId",
        },
      },
    };
  }
}

export default User;
