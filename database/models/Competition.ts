import { ICompetition } from "../../domain";
import BaseModel from "./BaseModel";
import CompetitionTeam from "./CompetitionTeam";
import Match from "./Match";
import UserCompetition from "./UserCompetition";

interface Competition extends ICompetition {}

class Competition extends BaseModel {
  static tableName = "competition";

  static get relationMappings() {
    return {
      matches: {
        relation: BaseModel.HasManyRelation,
        modelClass: Match,
        join: {
          from: "competition.id",
          to: "match.competitionId",
        },
      },
      userCompetitionList: {
        relation: BaseModel.HasManyRelation,
        modelClass: UserCompetition,
        join: {
          from: "competition.id",
          to: "user_competition.competitionId",
        },
      },
      teams: {
        relation: BaseModel.HasManyRelation,
        modelClass: CompetitionTeam,
        join: {
          from: "competition.id",
          to: "competition_team.competitionId",
        },
      },
    };
  }
}

export default Competition;
