import { ITeam } from "../../domain";
import BaseModel from "./BaseModel";
import CompetitionTeam from "./CompetitionTeam";

interface Team extends ITeam {}

class Team extends BaseModel {
  static tableName = "team";

  static get relationMappings() {
    return {
      competitions: {
        relation: BaseModel.HasManyRelation,
        modelClass: CompetitionTeam,
        join: {
          to: "team.id",
          from: "competition_team.teamId",
        },
      },
    };
  }
}

export default Team;
