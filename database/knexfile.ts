require("dotenv").config({ path: "../.env" });

const config = {
  client: "postgresql",
  connection: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    charset: "utf8",
    timezone: "+00:00",
  },
  pool: {
    min: 1,
    max: 10,
  },
  useNullAsDefault: true,
};

export { config };

module.exports = config;
