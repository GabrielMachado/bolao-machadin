select
    match.id id,
    host.name hostName,
    visitor.name visitorName,
    match.date date
from
    match
join competition_team hostInCompetition on hostInCompetition.id = match."hostId"
join competition_team visitorInCompetition on visitorInCompetition.id = match."visitorId"
join team host on host.id = hostInCompetition."teamId"
join team visitor on visitor.id = visitorInCompetition."teamId"
join bet on bet.id = match.id