import { Knex } from "knex";
import { createBets } from "../seeds/canarinhoHexa/createBatches";
import { createMatches } from "../seeds/canarinhoHexa/createMatches";
import { insertCompetition } from "../seeds/canarinhoHexa/insertCompetition";
import { insertTeams } from "../seeds/canarinhoHexa/insertTeams";
import { linkCompetitionTeam } from "../seeds/canarinhoHexa/linkCompetitionTeam";

export async function up(knex: Knex): Promise<void> {
  await insertCompetition(knex);
  await insertTeams(knex);
  await linkCompetitionTeam(knex);
  await createMatches(knex);
  await createBets(knex);
}

export async function down(knex: Knex): Promise<void> {
  await knex("user_competition_bet").delete();
  await knex("user_competition").delete();
  await knex("bet").delete();
  await knex("match").delete();
  await knex("competition_team").delete();
  await knex("competition").delete();
  await knex("team").delete();
  await knex("user").delete();
}
