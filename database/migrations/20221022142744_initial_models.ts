import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.schema
    .createTable("user", (table) => {
      table.bigIncrements("id").unsigned().primary();
      table.string("name").notNullable();
      table.string("email").notNullable().unique();
      table.string("password").notNullable();
      table.integer("claim").notNullable().defaultTo(1);

      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
    })
    .createTable("team", (table) => {
      table.bigIncrements("id").unsigned().primary();
      table.string("name").notNullable();
      table.string("logoUrl").nullable();

      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
    })
    .createTable("competition", (table) => {
      table.bigIncrements("id").unsigned().primary();
      table.string("name").notNullable();
      table.dateTime("startDate").notNullable();
      table.dateTime("endDate").notNullable();

      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
    })
    .createTable("competition_team", (table) => {
      table.bigIncrements("id").unsigned().primary();
      table
        .bigInteger("competitionId")
        .notNullable()
        .unsigned()
        .references("competition.id");
      table.bigInteger("teamId").notNullable().unsigned().references("team.id");

      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
    })
    .createTable("match", (table) => {
      table.bigIncrements("id").unsigned().primary();
      table
        .bigInteger("hostId")
        .notNullable()
        .unsigned()
        .references("competition_team.id");
      table
        .bigInteger("visitorId")
        .notNullable()
        .unsigned()
        .references("competition_team.id");
      table
        .bigInteger("competitionId")
        .notNullable()
        .unsigned()
        .references("competition.id");
      table.dateTime("date").notNullable();
      table.integer("hostScore").nullable();
      table.integer("visitorScore").nullable();

      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
    })
    .createTable("bet", (table) => {
      table.bigInteger("id").unsigned().primary().references("match.id");

      table.dateTime("startDate").notNullable();
      table.dateTime("endDate").notNullable();

      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
    })
    .createTable("user_competition", (table) => {
      table.bigIncrements("id").primary();
      table
        .bigInteger("competitionId")
        .notNullable()
        .unsigned()
        .references("competition.id");
      table.bigInteger("userId").notNullable().unsigned().references("user.id");
      table.boolean("status").notNullable().defaultTo(false);
      table.integer("totalPoints").notNullable().defaultTo(0);

      table.dateTime("createdAt").notNullable().defaultTo(knex.fn.now());
    })
    .createTable("user_competition_bet", (table) => {
      table.bigIncrements("id").primary();
      table
        .bigInteger("userCompetitionId")
        .notNullable()
        .unsigned()
        .references("user_competition.id");
      table.bigInteger("betId").notNullable().unsigned().references("bet.id");

      table.integer("hostScorePrediction").nullable();
      table.integer("visitorScorePrediction").nullable();
      table.integer("points").notNullable().defaultTo(0);
    });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema
    .dropTable("user_competition_bet")
    .dropTable("user_competition")
    .dropTable("bet")
    .dropTable("match")
    .dropTable("competition_team")
    .dropTable("competition")
    .dropTable("team")
    .dropTable("user");
}
