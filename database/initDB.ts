import BaseModel from "./models/BaseModel";
import knex, { Knex } from "knex";
import { logger } from "../utils/logger";

const resolveKnex = (): Knex => {
  // @ts-ignore
  if (!!global.knex) return global.knex;

  logger.info({
    context: "init db",
    message: "will create a new knex connection",
  });

  const knexconfig = require("./knexfile");
  const knexConnection = knex(knexconfig);

  // @ts-ignore
  global.knex = knexConnection;
  return knexConnection;
};

const initDB = () => {
  const knexConnection = resolveKnex();
  BaseModel.knex(knexConnection);

  return knex;
};

export default initDB;
